#include <QTest>
#include "myclasstest.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    return QTest::qExec(&MyClassTest(), argc, argv);
}
