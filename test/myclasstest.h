#ifndef MYCLASSTEST_H
#define MYCLASSTEST_H

#include <QtTest>
#include <myclass.h>

class MyClassTest : public QObject
{
    Q_OBJECT
public:
    MyClassTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void testAddition();

};

#endif // TESTAUTOCONNECT_H
