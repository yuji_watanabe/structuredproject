include(../src/src.pri)

QT += testlib
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

HEADERS += \
    myclasstest.h

SOURCES += \
    main.cpp \
    myclasstest.cpp

