#include <QCoreApplication>
#include <QTextStream>
#include <myclass.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MyClass myClass;
    QTextStream(stdout) << myClass.addition(1, 2);

    return a.exec();
}
